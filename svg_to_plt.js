let cutwidth   = 600;       //Cutting canvas width (mm)
let cutheight  = 600;       //Cutting canvas height (mm)
let resolution = 180;        //Ellipse and circle resolution (degrees)
let m          = 1;       //Arch resolution between 0 and 1, the smaller the number the bigger the resolution
let pi         = Math.PI;

let finder = require('fs-finder');                                                   //Starts npm(fs-finder) that allows the search of files within the computer
let files = finder.from('C:/Users/joset/Dropbox/enrique_jose_tow/svg-to-plt').findFiles('*.svg'); // C:/Users/joset/Dropbox/enrique_jose_tow/svg-to-plt
//Searches and generates an array with the paths of the svg files
let iterations = files.length;

for (let q = 0; q < iterations; q++)        //This will be done for every file in the directory
    {
    //svg to usable JSON array
    fs = require('fs');                         //Starts the file readinf module
    let svg = fs.readFileSync(files[q],'utf-8').toString(); //
// Reads the file in the array and transforms it into string
    svg = svg.slice(0,-7);         //Deletes </svg> at the end of the text
    let matriz = svg.split("<g"); //Transforms the string into an array spliting it every <g

    let inicio = matriz[0].split("<svg");      //artboard size
    inicio = inicio.splice(1);                  //deletes everything step by step until the only thing left is the artboard size
    inicio = inicio[0].split("<");
    let art =inicio[0].split(" ");
    let artboard = [];
    for (let w=0; w <art.length; w++)         //Looks for the element named viewBox to determine the size of it
    {
        if (art[w].slice(0,7) == 'viewBox'){
        let xart = Number(art[w+2]);
        let yart = Number(art[w+3].slice(0,-1));
        artboard.push(xart,yart);
    }
    }

    let ratio = cutwidth/artboard[0];

    matriz = matriz.splice(1);     //Removes the text at the beginning we won't need it anymore
    //console.log(matriz);

    let layers = matriz.length;     //Gets the amount of layers there are in the svg
    let salida = [];                //Creates the output array were we will drop all the layers

    for(let i=0; i < layers; i++)   //This will create the array with all the points
    {
        let array = matriz[i];      //Declares the segment of the array to work with it

        array = array.slice(0,-7);  //Elimina el texto y saltos de línea al final del texto
        array = array.split(">");   //Separa el array en dos 0=layer y 1 en adelante los puntos
        for (let lim=0; lim < array.length; lim++){
            array[lim]= array[lim].replace(/\r?\n|\r/,"");  //We remove any line jumps if they exist
            array[lim]= array[lim].replace(/\t\t/,"");
            array[lim]= array[lim].replace(/\t/,"");
            array[lim]= array[lim].replace(/\t"/,"");
        }
        let capa    = array[0];        //Crea una variable con el layer
        let resto   = array.splice(1); //Genera todos los trazos de la capa
        let trazos  = resto.length;    //Cuenta la cantidad de trazos en el layer
        let formas  = [];
        for (let j=0; j < trazos; j++) //Separador de formas
        {   let figura = resto[j];       //Establece el segmento de la matriz para trabajar

            figura = figura.slice(1,-1);   //Quita el texto inecesario al principio y final
            let trazo = figura.split(" "); //Lo separa en sus partes
            let points = [];               //Declara la variable points como matriz

            switch(trazo[0])        //All the cases for the forms
            {
            case 'line':                       //Case for lines
                let li=[];
                for(let w=0;w < trazo.length; w++)   //Looks for the x1 element which contains the first element
                {
                    if (trazo[w].slice(0,3)=='x1=')  //This pushes the location of x1 if found
                    {
                        li.push(w);
                    }
                }

                let p1 = [trazo[li[0]].slice(4,-1),  trazo[(li[0]+1)].slice(4,-1)].map(Number);  //Creates initial coordenates as numbers (x,y)
                let p2 = [trazo[(li[0]+2)].slice(4,-1),trazo[(li[0]+3)].slice(4,-1)].map(Number);  //Creates final coordenates as numbers (x,y)
                points.push(p1,p2);
                break;

            case 'circle':                         //Case for circles
                let cir=[];
                for(let w=0;w < trazo.length; w++)   //Looks for the cx element which contains the first element
                {
                    if (trazo[w].slice(0,3)=='cx=')  //This pushes the location of x1 if found
                    {
                        cir.push(w);
                    }
                }
                cir = cir[0];
                let centro = [trazo[cir].slice(4,-1),trazo[cir+1].slice(4,-1)].map(Number);  //Creates central coordinates as numbers (x,y)
                let radio  = Number(trazo[cir+2].slice(3,-1));                             //Creates the radius of the circle
                points.push(centro,radio);
                break;

            case 'polygon':                 //Case for polygons
                let po = [];                          //The coordinates of the path are always on the last item of the array, so we look for it
                for(let w=0;w < trazo.length; w++)   //Looks for the d= element which contains all the commands of the path
                {
                    if (trazo[w].slice(0,7)=='points=')  //This pushes the location of d= if found
                    {
                        po.push(w);
                    }
                }
                trazo[po]=trazo[po].slice(8);                 //Removes the points="
                let lastp = trazo.length-1;
                trazo[lastp]= trazo[lastp].slice(0,-1);             //We remove the last "
                for (let k=po; k < trazo.length; k++)        //This will store all the points into the array
                {

                    trazo[k]=trazo[k].replace(/\r?\n|\r/,"");       //Removes any line jump if they exist
                    trazo[k]=trazo[k].replace(/\t\t/,"");
                    let corner = trazo[k].split(",").map(Number);   //Creates the coordinates as numbers (x,y)
                    if (corner[0]=== null || corner[1]=== null)
                    {//Do nothing
                    }else{
                        points.push(corner);
                    }
                }

                points.push(trazo[po].split(",").map(Number));         //We push again the first point so it returns

                break;

            case 'ellipse':         //The case for ellipses is almost the same as for lines, the difference is where the points are located
                let el=[];
                for(let w=0;w < trazo.length; w++)   //Looks for the cx element which contains the first element
                {
                    if (trazo[w].slice(0,3)=='cx=')  //This pushes the location of x1 if found
                    {
                        el.push(w);
                    }
                }
                el = el[0];
                let center = [trazo[el].slice(4,-1),trazo[el+1].slice(4,-1)].map(Number);  //Creates the center coordinates as number
                let radius = [trazo[el+2].slice(4,-1),trazo[el+3].slice(4,-1)].map(Number);  //Creates the vertical and horizontal radius as numbers
                points.push(center,radius);
                break;

            case 'rect':            //The case for rectangles is almost the same as for lines, the difference is where the points are located
                let re=[];
                for(let w=0;w < trazo.length; w++)   //Looks for the cx element which contains the first element
                {
                    if (trazo[w].slice(0,2)=='x=')  //This pushes the location of x1 if found
                    {
                        re.push(w);
                    }
                }
                re = re[0];
                let centerRect = [trazo[re].slice(3,-1),trazo[re+1].slice(3,-1)].map(Number);  //Creates the initial coordinates as number
                let size       = [trazo[re+3].slice(7,-1),trazo[re+4].slice(8,-1)].map(Number);  //Creates the width and height as an array [w,h]
                points.push(centerRect,size);
                break;

            case 'path':            //Case for paths
                let p = [];                          //The coordinates of the path are always on the last item of the array, so we look for it
                for(let w=0;w < trazo.length; w++)   //Looks for the d= element which contains all the commands of the path
                {
                    if (trazo[w].slice(0,2)=='d=')  //This pushes the location of d= if found
                    {
                        p.push(w);
                    }
                }

                trazo[p] = trazo[p].slice(3);    //We remove the d="
                let q = trazo.length-1;
                trazo[q]= trazo[q].slice(0,-1); //We remove the last "
                let d = trazo;
                for (let k=p; k < trazo.length; k++)    //We want to split the routes into their commands,coordinates
                {

                    d[k]= d[k].replace(" ","");
                    let q = d[k].split(/(?=L|l|H|h|V|v|Z|z|C|c|S|s|Q|q|T|t|A|a)/);  //We split the route everytime we find a command letter
                    let steps = q.length;           //Reads how many commands there are
                    let comm = [];                  //Declares the variable comm as an array

                    for (let l=0; l < steps; l++)   //This will split the letter and the numbers into items of an array
                    {
                        let e = q[l].split("");     //This will split all the command into an array of characters, the one we want is the first

                        q[l]=q[l].slice(1);         //We then proceed to delete the letter from the command (we already stored the letter elsewhere
                        let w = q[l].split(/(?=-)|,/);//Then we split the command every , and - but keeps the -

                        let toomanyfors = w.length; //This is just to not create a subarray with the numbers
                        let stilltoomany = [];
                        stilltoomany.push(e[0]);    //We push the letter into the stilltoomany array

                        for (let m=0; m < toomanyfors; m++) //This for just pushes all the numbers to stilltoomany just to not create another subarray
                        {
                            stilltoomany.push(Number(w[m]));   //transforms into number and pushes, this creates a null on the command z but we'll ignore it
                        }

                        comm.push(stilltoomany);
                    }
                    points.push(comm);
                }
                break;

                case 'polyline':
                    let poly = [];                          //The coordinates of the path are always on the last item of the array, so we look for it
                    for(let w=0;w < trazo.length; w++)   //Looks for the d= element which contains all the commands of the path
                    {
                        if (trazo[w].slice(0,7)=='points=')  //This pushes the location of d= if found
                        {
                            poly.push(w);
                        }
                    }
                    trazo[poly]=trazo[poly].slice(8);                 //Removes the points="
                    let last = (trazo.length)-1;
                    trazo[last]= trazo[last].slice(0,-1);             //We remove the last "
                    for (let k=poly; k < trazo.length; k++)        //This will store all the points into the array the same way as in polygon
                    {

                            trazo[k]=trazo[k].replace(/\r?\n|\r/,"");       //Removes any line jump if they exist
                            trazo[k]=trazo[k].replace(/\t\t/,"");
                            let corner = trazo[k].split(",").map(Number);   //Creates the coordinates as numbers (x,y)
                            points.push(corner);
                    }
                    break;

                default:                //Default case
                    points.push("0");
            }

        let coord = {"type":trazo[0],"points":points}; //crea el objeto formas
        formas.push(coord);
        }
        let almost = {"name":capa.slice(4,-1),"paths":formas};  //We create an object to name all parts of it
        salida.push(almost);                                    //Then we push it to salida
    }
    let output = {"layer":salida};                              //We create our output and add a name to be able to find it

    //console.log(JSON.stringify(output));                      //The JSON with everything




    //This will write the plt file
    let name = files[q].slice(0,-4);    //Takes the name and path of the svg file and removes the extention .svg
    let filename = name+".plt";         //Adds the .plt extention using the same name and path

    let stream = fs.createWriteStream(filename);    //Opens a stream that allows to write constantly
    stream.once('open', function(fd)                //Sends the open instruction, once finished, it closes
    {
    let layerCount = output.layer.length;
    let plt =[];                                         //This will be used to calculate time
    for (let i = 0; i < layerCount; i++)        //First we'll write by layers
    {

        stream.write("IN;SC;PU;");                           //We write the beggining of the layer
        plt.push("IN;SC;PU;");

        //This choose what "SP" for the laser will be used
        if (output.layer[i].name.slice(-1) == "1")           //If the layer ends with a 1 then we'll use SP1
        {
            stream.write("SP1;LT;\n\n");
            plt.push("SP1;LT;\n\n");

        }else if (output.layer[i].name.slice(-1) == "2")     //If the layer ends with a 2 then we'll use SP2
        {
            stream.write("SP2;LT;\n\n");
            plt.push("SP2;LT;\n\n");

        }else if (output.layer[i].name.slice(-1) == "3")     //If the layer ends with a 3 then we'll use SP3
        {
            stream.write("SP3;LT;\n\n");
            plt.push("SP3;LT;\n\n");

        }else if (output.layer[i].name.slice(-1) == "2")     //If the layer ends with a 4 then we'll use SP4
        {
            stream.write("SP4;LT;\n\n");
            plt.push("SP4;LT;\n\n");


        }else                                                //Everything else will be SP5
        {
            stream.write("SP5;LT;\n\n");
            plt.push("SP5;LT;\n\n");
        }

        let pathCount = output.layer[i].paths.length;
        for (let j=0; j < pathCount; j++)                       //Every layer will be written separately
        {
         switch(output.layer[i].paths[j].type)                  //This draws all the figures
         {
             case 'line':                                                   //Line drawer
                 let p1 = output.layer[i].paths[j].points[0];               //Searches the start coordinates
                 let x1 = Math.round(p1[0]*ratio*40);
                 let y1 = Math.round((cutheight-(p1[1]*ratio))*40);           //Flips y coordinate
                 let p2 = output.layer[i].paths[j].points[1];
                 let x2 = Math.round(p2[0]*ratio*40);
                 let y2 = Math.round((cutheight-(p2[1]*ratio))*40);
                 stream.write("PW1\n");
                 stream.write("PU"+JSON.stringify(x1)+","+JSON.stringify(y1)+";\n");
                 stream.write("PD"+JSON.stringify(x2)+","+JSON.stringify(y2)+";\n");
                 plt.push("PW1\n");
                 plt.push("PU"+JSON.stringify(x1)+","+JSON.stringify(y1)+";\n");
                 plt.push("PD"+JSON.stringify(x2)+","+JSON.stringify(y2)+";\n");
              break;

             case 'circle':                                                   //Circle drawer
                 let centre = output.layer[i].paths[j].points[0];             //Searches, declares and converts to lasercutter units the center and radius
                 let x      = centre[0]*ratio*40;
                 let y      = (cutheight-(centre[1]*ratio))*40;
                 let r      = output.layer[i].paths[j].points[1]*ratio*40;

                 let inicio = [Math.round(x+r),Math.round(y)];                 //Creates the starting point
                 stream.write("PW1\n");
                 stream.write("PU"+JSON.stringify(inicio).replace("[","").replace("]","")+";\n");
                 plt.push("PW1\n");
                 plt.push("PU"+JSON.stringify(inicio).replace("[","").replace("]","")+";\n");

                 for(let k=resolution; k < (360+resolution); k+=resolution) //We will draw a line every x degrees (resolution given)
                 {
                     let rad = k*pi/180;
                     let xc = Math.round(x + r*Math.cos(rad));          //We use the circle parametric formula x1=x0+rcos(th);
                     let yc = Math.round(y + r*Math.sin(rad));          //We use the circle parametric formula y1=y0+rsin(th);

                     stream.write("PD"+JSON.stringify(xc)+","+JSON.stringify(yc)+";\n");
                     plt.push("PD"+JSON.stringify(xc)+","+JSON.stringify(yc)+";\n");
                 }
                 break;

             case 'polygon':                        //Polygon drawer
                 let startp = output.layer[i].paths[j].points[0];           //Does the same thing as the line function
                 let xp = Math.round(startp[0]*ratio*40);
                 let yp = Math.round((cutheight-(startp[1]*ratio))*40);
                 stream.write("PW1\n");
                 stream.write("PU"+JSON.stringify(xp)+","+JSON.stringify(yp)+";\n");
                 plt.push("PW1\n");
                 plt.push("PU"+JSON.stringify(xp)+","+JSON.stringify(yp)+";\n");

                 let pointCount = output.layer[i].paths[j].points.length;   //The only difference is that, since there are more points, they are written in a for loop
                 for (let k=1; k < pointCount; k++)
                 {
                     let pp  = output.layer[i].paths[j].points[k];
                     let xpp = Math.round(pp[0]*cutwidth/artboard[0]*40);
                     let ypp = Math.round((cutheight-(pp[1]*ratio))*40);
                     stream.write("PD"+JSON.stringify(xpp)+","+JSON.stringify(ypp)+";\n");
                     plt.push("PD"+JSON.stringify(xpp)+","+JSON.stringify(ypp)+";\n");
                 }
                 break;

             case 'ellipse':
                 let centreE  = output.layer[i].paths[j].points[0];             //Searches, declares and converts to lasercutter units the center and radius
                 let xe       = centreE[0]*ratio*40;
                 let ye       = (cutheight-(centreE[1]*ratio))*40;
                 let radiusE  = output.layer[i].paths[j].points[1];
                 let rx       = radiusE[0]*ratio*40;
                 let ry       = radiusE[1]*ratio*40;
                 stream.write("PW1\n");
                 stream.write("PU"+JSON.stringify(Math.round(xe+rx))+","+JSON.stringify(Math.round(ye))+";\n");
                 plt.push("PW1\n");
                 plt.push("PU"+JSON.stringify(Math.round(xe+rx))+","+JSON.stringify(Math.round(ye))+";\n");

                 for(let k=resolution; k < (360+resolution); k+=resolution) //We will draw a line every x degrees (resolution given)
                 {
                     let rad = k*pi/180;
                     let xc = Math.round(xe + rx*Math.cos(rad));          //We use the circle parametric formula x1=x0+rxcos(th);
                     let yc = Math.round(ye + ry*Math.sin(rad));          //We use the circle parametric formula y1=y0+rysin(th);

                     stream.write("PD"+JSON.stringify(xc)+","+JSON.stringify(yc)+";\n");
                     plt.push("PD"+JSON.stringify(xc)+","+JSON.stringify(yc)+";\n");
                 }
                 break;

             case 'rect':                   //Rectangle drawer
                 let centreR  = output.layer[i].paths[j].points[0];             //Searches, declares and converts to lasercutter units the initial point and width,height
                 let xR       = centreR[0]*ratio*40;
                 let yR       = (cutheight-(centreR[1]*ratio))*40;
                 let radiusR  = output.layer[i].paths[j].points[1];
                 let w        = radiusR[0]*ratio*40;
                 let h        = radiusR[1]*ratio*40;

                 stream.write("PW1\n");
                 stream.write("PU"+JSON.stringify(Math.round(xR))  +","+JSON.stringify(Math.round(yR))  +";\n");    //Draws every line
                 stream.write("PD"+JSON.stringify(Math.round(xR+w))+","+JSON.stringify(Math.round(yR))  +";\n");
                 stream.write("PD"+JSON.stringify(Math.round(xR+w))+","+JSON.stringify(Math.round(yR-h))+";\n");
                 stream.write("PD"+JSON.stringify(Math.round(xR))  +","+JSON.stringify(Math.round(yR-h))+";\n");
                 stream.write("PD"+JSON.stringify(Math.round(xR))  +","+JSON.stringify(Math.round(yR))  +";\n");
                 plt.push("PW1\n");
                 plt.push("PU"+JSON.stringify(Math.round(xR))  +","+JSON.stringify(Math.round(yR))  +";\n");    //Draws every line
                 plt.push("PD"+JSON.stringify(Math.round(xR+w))+","+JSON.stringify(Math.round(yR))  +";\n");
                 plt.push("PD"+JSON.stringify(Math.round(xR+w))+","+JSON.stringify(Math.round(yR-h))+";\n");
                 plt.push("PD"+JSON.stringify(Math.round(xR))  +","+JSON.stringify(Math.round(yR-h))+";\n");
                 plt.push("PD"+JSON.stringify(Math.round(xR))  +","+JSON.stringify(Math.round(yR))  +";\n");
                 break;

             case 'path':           //path drawer
                 let pathCount  = output.layer[i].paths[j].points.length;         //Counts how many subPaths there are
                 for (let k=0; k < pathCount; k++)                                //This will be executed for every "subPath"
                 {
                     let subPath = output.layer[i].paths[j].points[k];            //We declare the subpath to be able to search the array items
                     let commM     = subPath[0];                                  //We declare the first command of the path (the M one) and print it
                     let xM        = Math.round(commM[1]*ratio*40);
                     let yM        = Math.round((cutheight-(commM[2]*ratio))*40);
                     stream.write("PW1\n");
                     stream.write("PU"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");
                     plt.push("PW1\n");
                     plt.push("PU"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");

                     let subPathCount = subPath.length;                           //Checks how many commands the subPath has
                     let lastPosition = [xM,yM];                                  //This will the last position, used for relative position commands
                     let lastBezier   = [];                                       //This is used the store the last Bezier point to be used in commands S and T


                     for (let l=1; l < subPathCount; l++)   //We will write each command one by one
                     {
                         let comm = subPath[l];
                         switch (comm[0])                   //This switch will write depending on the command
                         {
                             case 'M':  //Absoulte M command in case the splicer didn't separate a subPath
                                 xM = Math.round(comm[1]*ratio*40);                                 //This resets the initial points
                                 yM = Math.round((cutheight-(comm[2]*ratio))*40);

                                 stream.write("PU"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print the new M
                                 plt.push("PU"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");
                                 lastPosition = [xM,yM];                                                //Also we store it as the last value
                                 break;

                             case'm':   //Relative m command in case the splicer didn't separate a subPath
                                 xM = Math.round(lastPosition[0]+(comm[1]*ratio*40));               //This resets the initial points
                                 yM = Math.round(lastPosition[1]-(comm[2]*ratio*40));

                                 stream.write("PU"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print the new M
                                 plt.push("PU"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print the new M

                                 lastPosition = [xM,yM];                                                //Also we store it as the last value
                                 break;

                             case 'L':  //Absolute line command
                                 let lx = Math.round(comm[1]*ratio*40);                  //Gets the coordinate in lasercutter units
                                 let ly = Math.round((cutheight-(comm[2]*ratio))*40);
                                 lastPosition = [lx,ly];                                                //Stores the position in case it's needed

                                 stream.write("PD"+JSON.stringify(lx)+","+JSON.stringify(ly)+";\n");    //writes in plt
                                 plt.push("PD"+JSON.stringify(lx)+","+JSON.stringify(ly)+";\n");    //writes in plt
                                 break;

                             case'l':   //Relative line command
                                 let lrx = Math.round(lastPosition[0]+(comm[1]*ratio*40));             //Gets the number from the command and adds it to the last value
                                 let lry = Math.round(lastPosition[1]-(comm[2]*ratio*40));
                                 //Since the Y coordinate is already flipped inside the lastPosition value and comm[1] is relative, the Y is not flipped here

                                 lastPosition = [lrx,lry];                                  //Stores the new position

                                 stream.write("PD"+JSON.stringify(lrx)+","+JSON.stringify(lry)+";\n");
                                 plt.push("PD"+JSON.stringify(lrx)+","+JSON.stringify(lry)+";\n");
                                 break;

                             case'H':      //Absolute horizontal line command
                                 let xH = Math.round(comm[1]*ratio*40); //Since it's horizontal, there's no vertical movement
                                 let yH = Math.round(lastPosition[1]);              //We keep the last y because it only moves in X
                                 lastPosition = [xH,yH];

                                 stream.write("PD"+JSON.stringify(xH)+","+JSON.stringify(yH)+";\n");
                                 plt.push("PD"+JSON.stringify(xH)+","+JSON.stringify(yH)+";\n");
                                 break;

                             case'h':   //Relative horizontal line command
                                 let xh = Math.round(lastPosition[0]+(comm[1]*ratio*40)); //It's the same but adding
                                 let yh = Math.round(lastPosition[1]);
                                 lastPosition = [xh,yh];

                                 stream.write("PD"+JSON.stringify(xh)+","+JSON.stringify(yh)+";\n");
                                 plt.push("PD"+JSON.stringify(xh)+","+JSON.stringify(yh)+";\n");
                                 break;

                             case 'V':  //Absolute vertical line command
                                 let yV = Math.round((cutheight-(comm[1]*ratio))*40); //Since it's vertical, there's no horizontal movement
                                 let xV = Math.round(lastPosition[0]);                          //We keep the last x because it only moves in y
                                 lastPosition = [xV,yV];

                                 stream.write("PD"+JSON.stringify(xV)+","+JSON.stringify(yV)+";\n");
                                 plt.push("PD"+JSON.stringify(xV)+","+JSON.stringify(yV)+";\n");
                                 break;

                             case 'v':
                                 let yv = Math.round(lastPosition[1]-(comm[1]*ratio*40)); //It's the same but adding
                                 let xv = Math.round(lastPosition[0]);
                                 lastPosition = [xv,yv];

                                 stream.write("PD"+JSON.stringify(xv)+","+JSON.stringify(yv)+";\n");
                                 plt.push("PD"+JSON.stringify(xv)+","+JSON.stringify(yv)+";\n");
                                 break;

                             case 'Z':
                                 stream.write("PD"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print M again
                                 plt.push("PD"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print M again
                                 lastPosition = [xM,yM];                                                //Also we store it as the last value in case it's needed
                                 break;

                             case 'z':      //z or Z it's a return to M command
                                 stream.write("PD"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print M again
                                 plt.push("PD"+JSON.stringify(xM)+","+JSON.stringify(yM)+";\n");    //We print M again
                                 lastPosition = [xM,yM];                                                //Also we store it as the last value in case it's needed
                                 break;

                             case 'C':     //Absolute cubic Bezier curve command
                                 let C1 = lastPosition;             //The starting point is the last position which is already marked

                                 let p1 = [comm[1]*ratio*40,(cutheight-(comm[2]*ratio))*40];
                                 let p2 = [comm[3]*ratio*40,(cutheight-(comm[4]*ratio))*40];    //We convert the points into lasercut units
                                 let p3 = [comm[5]*ratio*40,(cutheight-(comm[6]*ratio))*40];

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xC = Math.round(Math.pow(1-t,3)*C1[0]+Math.pow(1-t,2)*3*t*p1[0]+(1-t)*3*t*t*p2[0]+t*t*t*p3[0]);
                                     let yC = Math.round(Math.pow(1-t,3)*C1[1]+Math.pow(1-t,2)*3*t*p1[1]+(1-t)*3*t*t*p2[1]+t*t*t*p3[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xC)+","+JSON.stringify(yC)+";\n");
                                     plt.push("PD"+JSON.stringify(xC)+","+JSON.stringify(yC)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(p3[0]))+","+JSON.stringify(Math.round(p3[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(p3[0]))+","+JSON.stringify(Math.round(p3[1]))+";\n");  //We print the final point
                                 lastPosition = [p3[0],p3[1]];                                              //The final point is stored
                                 lastBezier   = [p2[0]-p3[0],p2[1]-p3[1]];                                  //We store the relative position of p2 to p1m in case a command S is used later
                                 break;

                             case 'c':
                                 let c1 = lastPosition;             //The starting point is the last position which is already marked

                                 let pc1 = [c1[0]+(comm[1]*ratio*40),c1[1]-(comm[2]*ratio*40)];
                                 let pc2 = [c1[0]+(comm[3]*ratio*40),c1[1]-(comm[4]*ratio*40)];  //We change the relative positions for real ones
                                 let pc3 = [c1[0]+(comm[5]*ratio*40),c1[1]-(comm[6]*ratio*40)];

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xc = Math.round(Math.pow(1-t,3)*c1[0]+Math.pow(1-t,2)*3*t*pc1[0]+(1-t)*3*t*t*pc2[0]+t*t*t*pc3[0]);
                                     let yc = Math.round(Math.pow(1-t,3)*c1[1]+Math.pow(1-t,2)*3*t*pc1[1]+(1-t)*3*t*t*pc2[1]+t*t*t*pc3[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xc)+","+JSON.stringify(yc)+";\n");
                                     plt.push("PD"+JSON.stringify(xc)+","+JSON.stringify(yc)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(pc3[0]))+","+JSON.stringify(Math.round(pc3[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(pc3[0]))+","+JSON.stringify(Math.round(pc3[1]))+";\n");  //We print the final point
                                 lastPosition = [pc3[0],pc3[1]];                                              //The final point is stored
                                 lastBezier   = [pc2[0]-pc3[0],pc2[1]-pc3[1]];                                //We store the relative position of p2 to p1 in case a command S is used later
                                 break;

                             case'S':
                                 let S1 = lastPosition;             //The starting point is the last position which is already marked

                                 let pS1 = [S1[0]-lastBezier[0],S1[1]-lastBezier[1]];  //P1 is a mirrored version of the lasBezier

                                 let pS2 = [comm[1]*ratio*40,(cutheight-(comm[2]*ratio))*40];    //We convert the points into lasercut units
                                 let pS3 = [comm[3]*ratio*40,(cutheight-(comm[4]*ratio))*40];

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xS = Math.round(Math.pow(1-t,3)*S1[0]+Math.pow(1-t,2)*3*t*pS1[0]+(1-t)*3*t*t*pS2[0]+t*t*t*pS3[0]);
                                     let yS = Math.round(Math.pow(1-t,3)*S1[1]+Math.pow(1-t,2)*3*t*pS1[1]+(1-t)*3*t*t*pS2[1]+t*t*t*pS3[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xS)+","+JSON.stringify(yS)+";\n");
                                     plt.push("PD"+JSON.stringify(xS)+","+JSON.stringify(yS)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(pS3[0]))+","+JSON.stringify(Math.round(pS3[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(pS3[0]))+","+JSON.stringify(Math.round(pS3[1]))+";\n");
                                 lastPosition = [pS3[0],pS3[1]];                                              //The final point is stored
                                 lastBezier   = [pS2[0]-pS3[0],pS2[1]-pS3[1]];                                  //We store the relative position of p2 to p1 in case a command S is used later
                                 break;

                             case 's':
                                 let s1 = lastPosition;             //The starting point is the last position which is already marked

                                 let ps1 = [s1[0]-lastBezier[0],s1[1]-lastBezier[1]];  //P1 is a mirrored version of the lasBezier

                                 let ps2 = [s1[0]+(comm[1]*ratio*40),s1[1]-(comm[2]*ratio*40)];  //We change the relative positions for real ones
                                 let ps3 = [s1[0]+(comm[3]*ratio*40),s1[1]-(comm[4]*ratio*40)];

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xs = Math.round(Math.pow(1-t,3)*s1[0]+Math.pow(1-t,2)*3*t*ps1[0]+(1-t)*3*t*t*ps2[0]+t*t*t*ps3[0]);
                                     let ys = Math.round(Math.pow(1-t,3)*s1[1]+Math.pow(1-t,2)*3*t*ps1[1]+(1-t)*3*t*t*ps2[1]+t*t*t*ps3[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xs)+","+JSON.stringify(ys)+";\n");
                                     plt.push("PD"+JSON.stringify(xs)+","+JSON.stringify(ys)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(ps3[0]))+","+JSON.stringify(Math.round(ps3[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(ps3[0]))+","+JSON.stringify(Math.round(ps3[1]))+";\n");  //We print the final point
                                 lastPosition = [ps3[0],ps3[1]];                                              //The final point is stored
                                 lastBezier   = [ps2[0]-ps3[0],ps2[1]-ps3[1]];                                //We store the relative position of p2 to p1 in case a command S is used later
                                 break;

                             case 'Q':
                                 let Q1 = lastPosition;             //The starting point is the last position which is already marked

                                 let pQ1 = [comm[1]*ratio*40,(cutheight-(comm[2]*ratio))*40];
                                 let pQ2 = [comm[3]*ratio*40,(cutheight-(comm[4]*ratio))*40];    //We convert the points into lasercut units

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xQ = Math.round(Math.pow(1-t,2)*Q1[0]+(1-t)*2*t*pQ1[0]+t*t*pQ2[0]);
                                     let yQ = Math.round(Math.pow(1-t,2)*Q1[1]+(1-t)*2*t*pQ1[1]+t*t*pQ2[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xQ)+","+JSON.stringify(yQ)+";\n");
                                     plt.push("PD"+JSON.stringify(xQ)+","+JSON.stringify(yQ)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(pQ2[0]))+","+JSON.stringify(Math.round(pQ2[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(pQ2[0]))+","+JSON.stringify(Math.round(pQ2[1]))+";\n");  //We print the final point
                                 lastPosition = [pQ2[0],pQ2[1]];                                              //The final point is stored
                                 lastBezier   = [pQ1[0]-pQ2[0],pQ1[1]-pQ2[1]];                                //We store the relative position of p2 to p1 in case a command T is used later
                                 break;

                             case 'q':
                                 let q1 = lastPosition;             //The starting point is the last position which is already marked

                                 let pq1 = [q1[0]+(comm[1]*ratio*40),q1[1]-(comm[2]*ratio*40)];
                                 let pq2 = [q1[0]+(comm[3]*ratio*40),q1[1]-(comm[4]*ratio*40)];  //We change the relative positions for real ones

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xq = Math.round(Math.pow(1-t,2)*q1[0]+(1-t)*2*t*pq1[0]+t*t*pq2[0]);
                                     let yq = Math.round(Math.pow(1-t,2)*q1[1]+(1-t)*2*t*pq1[1]+t*t*pq2[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xq)+","+JSON.stringify(yq)+";\n");
                                     plt.push("PD"+JSON.stringify(xq)+","+JSON.stringify(yq)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(pq2[0]))+","+JSON.stringify(Math.round(pq2[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(pq2[0]))+","+JSON.stringify(Math.round(pq2[1]))+";\n");  //We print the final point
                                 lastPosition = [pq2[0],pq2[1]];                                              //The final point is stored
                                 lastBezier   = [pq1[0]-pq2[0],pq1[1]-pq2[1]];                                //We store the relative position of p2 to p1 in case a command T is used later
                                 break;

                             case 'T':
                                 let T1 = lastPosition;             //The starting point is the last position which is already marked

                                 let pT1 = [T1[0]-lastBezier[0],T1[1]-lastBezier[1]];  //P1 is a mirrored version of the lasBezier

                                 let pT2 = [comm[1]*ratio*40,(cutheight-(comm[2]*ratio))*40];    //We convert the points into lasercut units

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xT = Math.round(Math.pow(1-t,2)*T1[0]+(1-t)*2*t*pT1[0]+t*t*pT2[0]);
                                     let yT = Math.round(Math.pow(1-t,2)*T1[1]+(1-t)*2*t*pT1[1]+t*t*pT2[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xT)+","+JSON.stringify(yT)+";\n");
                                     plt.push("PD"+JSON.stringify(xT)+","+JSON.stringify(yT)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(pT2[0]))+","+JSON.stringify(Math.round(pT2[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(pT2[0]))+","+JSON.stringify(Math.round(pT2[1]))+";\n");  //We print the final point
                                 lastPosition = [pT2[0],pT2[1]];                                              //The final point is stored
                                 lastBezier   = [pT1[0]-pT2[0],pQ1[1]-pT2[1]];                                //We store the relative position of p2 to p1 in case a command T is used later
                                 break;

                             case 't':
                                 let t1 = lastPosition;             //The starting point is the last position which is already marked

                                 let pt1 = [t1[0]-lastBezier[0],t1[1]-lastBezier[1]];  //P1 is a mirrored version of the lasBezier

                                 let pt2 = [t1[0]+(comm[3]*ratio*40),t1[1]-(comm[4]*ratio*40)];  //We change the relative positions for real ones

                                 for (let t = m; t < 1; t+=m)       //We will draw as many lines as the resolution requires
                                 {
                                     let xt = Math.round(Math.pow(1-t,2)*t1[0]+(1-t)*2*t*pt1[0]+t*t*pt2[0]);
                                     let yt = Math.round(Math.pow(1-t,2)*t1[1]+(1-t)*2*t*pt1[1]+t*t*pt2[1]);
                                     //Cubic Bezier Formula

                                     stream.write("PD"+JSON.stringify(xt)+","+JSON.stringify(yt)+";\n");
                                     plt.push("PD"+JSON.stringify(xt)+","+JSON.stringify(yt)+";\n");
                                 }

                                 stream.write("PD"+JSON.stringify(Math.round(pt2[0]))+","+JSON.stringify(Math.round(pt2[1]))+";\n");  //We print the final point
                                 plt.push("PD"+JSON.stringify(Math.round(pt2[0]))+","+JSON.stringify(Math.round(pt2[1]))+";\n");  //We print the final point
                                 lastPosition = [pt2[0],pt2[1]];                                              //The final point is stored
                                 lastBezier   = [pt1[0]-pt2[0],pt1[1]-pt2[1]];                                //We store the relative position of p2 to p1 in case a command T is used later
                                 break;

                             case 'A':  //For future programming at the moment it just moves without cutting to the final point
                                 let xA = (comm[6]*ratio*40);
                                 let yA = ((cutheight-(comm[7]*ratio))*40);

                                 stream.write("PU"+JSON.stringify(xA)+","+JSON.stringify(yA)+";\n");
                                 plt.push("PU"+JSON.stringify(xA)+","+JSON.stringify(yA)+";\n");
                                 lastPosition = [xA,yA];
                                 break;

                             case 'a':  //For future programming at the moment it just moves without cutting to the final point
                                 let xa = (lastPosition[0]+(comm[6]*ratio*40));
                                 let ya = (lastPosition[1]-(comm[7]*ratio*40));

                                 stream.write("PU"+JSON.stringify(xa)+","+JSON.stringify(ya)+";\n");
                                 plt.push("PU"+JSON.stringify(xa)+","+JSON.stringify(ya)+";\n");
                                 lastPosition = [xa,yA];
                                 break;

                             default:
                                 stream.write("PU0,0;\n");
                                 plt.push("PU0,0;\n");
                         }
                     }
                 }
                 break;

             case '´polyline':              //Polyline drawer
                 let startpoly = output.layer[i].paths[j].points[0];           //Does exactly the same thing as the polygon function
                 let xpo = Math.round(startpoly[0]*ratio*40);
                 let ypo = Math.round((cutheight-(startpoly[1]*ratio))*40);
                 stream.write("PW1\n");
                 stream.write("PU"+JSON.stringify(xpo)+","+JSON.stringify(ypo)+";\n");
                 plt.push("PW1\n");
                 plt.push("PU"+JSON.stringify(xpo)+","+JSON.stringify(ypo)+";\n");

                 let pointsCount = output.layer[i].paths[j].points.length;
                 for (let k=1; k < pointsCount; k++)
                 {
                     let ppo  = output.layer[i].paths[j].points[k];
                     let xppo = Math.round(ppo[0]*cutwidth/artboard[0]*40);
                     let yppo = Math.round((cutheight-(ppo[1]*ratio))*40);
                     stream.write("PD"+JSON.stringify(xppo)+","+JSON.stringify(yppo)+";\n");
                     plt.push("PD"+JSON.stringify(xppo)+","+JSON.stringify(yppo)+";\n");
                 }
                 break;

             default:               //If there's a line type that doesn't match, it doesn't do anything
         }


            stream.write("\n");       //Adds a line jump for ease of reading
            plt.push("\n");       //Adds a line jump for ease of reading
        }
        stream.write("PU;SP;EC;PG1;EC1;OE\n\n");             //We write the ending of the layer
        plt.push("PU;SP;EC;PG1;EC1;OE\n\n");             //We write the ending of the layer
    }



        let drawSpeed = 0;                    //This will calculate the drawing time
        let drawTime = [];
        let storedTime = 0;
        let startPoint = [0, 0];
        let penUpSpeed = 40*1800;

        for (let i = 0; i < plt.length; i++) {
            if (plt[i].slice(0, 2) == 'SP')      //First checks if it's a new layer to change the speed
            {
                drawTime.push("#" + plt[i].slice(0, 3)+" ");  //Pushes the name of the layer
                if (plt[i].slice(2, 3) == '1') {
                    drawSpeed = 40 * 500;         //We insert the speed in mm/s and multiply by 40 which converts it into lasercut units
                }
                else {
                    drawSpeed = 40 * 10;       //More speeds can be added, in this case is not needed
                }
            }
            else if (plt[i].slice(0,1) == 'P' && plt[i].slice(1,2) != 'W' && isNaN(Number(plt[i].slice(2, 3))) === false)    //If the third character is a number
            {

                let finalPoints =[];
                finalPoints = plt[i].split(",");             //Separates x and y coordinates
                let Xfinal = Number(finalPoints[0].slice(2));    //converts the coordinates to number
                let Yfinal = Number(finalPoints[1].slice(0,-2));


                let dispVect = [Xfinal - startPoint[0], Yfinal - startPoint[1]];   //Creates the vector to be measured
                let dispMag = Math.sqrt(Math.pow(dispVect[0], 2) + Math.pow(dispVect[1], 2));  //Gets the magnitude of the vector
                startPoint = dispVect;

                if (finalPoints[0].slice(0, 2) == 'PU')       //For PU commands
                {
                    let time = (penUpSpeed / dispMag); //Calculates the time of movement
                    storedTime = storedTime + time;  //Adds it to the stored time
                }
                else                                      //For PD commands
                {
                    let time = (drawSpeed / dispMag); //Calculates the time of movement
                    storedTime = storedTime + time;  //Adds it to the stored time
                    console.log(storedTime);
                }
            }
            else if (plt[i].slice(0,9) == 'PU;SP;EC;')      //If it finds the end of the layer
            {
                drawTime.push(JSON.stringify(Math.round(storedTime)) + "seg \n");                //Sends the stored time to the array
                storedTime = 0;                                                                 //Resets the stored time
                console.log("\n\n\n")
            }
        }
        let pltFinal = "";                        //We declare pltDrawing as a string
        for (let i=0; i < drawTime.length; i++)   //Writes the times to pltFinal
        {
            pltFinal = pltFinal+drawTime[i];           //Writes the commands into the plt
        }

        stream.write(pltFinal+"\n");


    });

}
console.log("Finished, file(s) exported to .plt");  //Displays a finished log